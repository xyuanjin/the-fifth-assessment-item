package controller

import (
	"accountingsystem/model"
	"accountingsystem/service"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func SelectUserByUserName(w http.ResponseWriter, r *http.Request) {
	// http://localhost:8000/login?id=1&param2=3
	// map[string][]string
	// "id":["1"],"param2":["3"]
	if r.Method == "GET" {
		//解析tmpl中的内容
		t1, err := template.ParseFiles("./web/selectuser.html")
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
		err = t1.Execute(w, "")
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
	} else {
		//解析tmpl中的内容
		t, err := template.ParseFiles("./web/selectuser.html")
		if err != nil {
			log.Fatalln(err.Error())
			return
		}

		//解析表单
		err = r.ParseForm()
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
		//获取表单信息用户名
		name := r.Form["name"][0]
		//根据用户名查询信息
		user := service.SelectUserByUserName(name)
		if user.IsStructureEmpty() {
			err = t.Execute(w, "该用户不存在！")
			if err != nil {
				log.Fatalln(err.Error())
				return
			}
		} else {
			//将用户信息返回页面
			err = t.Execute(w, user)
			if err != nil {
				log.Fatalln(err.Error())
				return
			}
		}
	}

}
func SelectAllUser(w http.ResponseWriter, r *http.Request) {
	//解析alluser.html中的内容
	t, err := template.ParseFiles("./web/alluser.html")
	if err != nil {
		log.Fatalln(err.Error())
		return
	}
	result := service.SelectAllUser()
	//赋值给tmpl中的待定值  将data赋给w
	err = t.Execute(w, result)
	if err != nil {
		log.Fatalln(err.Error())
		return
	}
}

func InsertUser(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		//解析register.html中的内容
		t, err := template.ParseFiles("./web/register.html")
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
		err = t.Execute(w, "")
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
	} else {
		//解析register.html中的内容
		t, err := template.ParseFiles("./web/register.html")
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
		//解析表单
		err = r.ParseForm()
		if err != nil {
			log.Fatalln(err.Error())
		}
		var result string
		//获取表单内容
		userName := r.Form["userName"][0]
		password := r.Form["password"][0]
		var user model.User
		//判断用户名是否重复
		users := service.SelectAllUser()
		for _, i := range users {
			if i.Name == userName {
				err = t.Execute(w, "该用户名已存在，请重新输入！")
				if err != nil {
					log.Fatalln(err.Error())
					return
				}
				return
			}
			if i == users[len(users)-1] {
				user = model.User{
					Name:     userName,
					Password: password,
				}
				if service.InsertUser(user) {
					result = "注册成功！"
				} else {
					result = "很抱歉，注册失败！"
				}
				//赋值给tmpl中的待定值  将data赋给w
				err = t.Execute(w, result)
				if err != nil {
					log.Fatalln(err.Error())
					return
				}

			}
		}
	}
}


func UpdateUserByUsername(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		//解析updateuser.html中的内容
		t, err := template.ParseFiles("./web/updateuser.html")
		if err != nil {
			log.Fatalln(err.Error())
		}
		//{{.}}不需要赋值，也不需要显示出来
		err = t.Execute(w, "")
		if err != nil {
			log.Fatalln(err.Error())
		}
		return
	} else {
		//解析updateuser.html
		t1, err := template.ParseFiles("./web/updateuser.html")
		//解析form表单
		err = r.ParseForm()
		if err != nil {
			log.Fatalln(err.Error())
		}
		//获取表单中的内容
		userName := r.Form["userName"][0]
		password := r.Form["password"][0]
		//删除原来用户
		service.DeleteUserByUserName(userName)
		//将获取内容赋值给结构体user
		user:= model.User{
			Name : userName,
			Password: password,
		}
		//判断是否添加成功
		if service.InsertUser(user) {
			err = t1.Execute(w, "修改成功！")
			if err != nil {
				log.Fatalln(err.Error())
			}
		} else {
			err = t1.Execute(w, "修改失败，请重新输入！")
			if err != nil {
				log.Fatalln(err.Error())
			}
		}
	}
}

func DeleteUserByUserName(w http.ResponseWriter, r *http.Request) {

}

func Login(w http.ResponseWriter, r *http.Request) {
	//浏览器首页输入发出GET请求
	if r.Method == "GET" {
		//解析login.tmpl中的内容
		t1, err := template.ParseFiles("./web/login.html")
		if err != nil {
			log.Fatalln(err.Error())
		}
		//{{.}}不需要赋值，也不需要显示出来
		err = t1.Execute(w, "")
		if err != nil {
			log.Fatalln(err.Error())
		}
		return
	} else { //页面提交登录发出post请求
		//解析login.tmpl中的内容
		t1, err := template.ParseFiles("./web/login.html")
		if err != nil {
			log.Fatalln(err.Error())
		}
		//解析form表单
		err = r.ParseForm()
		if err != nil {
			log.Fatalln(err.Error())
		}
		//获取form表单中的内容,获取的值为string类型
		//用户名
		name := r.Form["name"][0]
		if err != nil {
			log.Fatalln(err.Error())
		}
		//用户密码
		password := r.Form["password"][0]
		if err != nil {
			log.Println(err.Error())
		}
		//根据输入的用户名查询到的用户结构体
		user := service.SelectUserByUserName(name)
		//用户不存在
		if user.IsStructureEmpty() {
			//给{{.}}赋值，说明用户不存在，重新登录
			err = t1.Execute(w, "该用户不存在！")
			if err != nil {
				log.Println(err.Error())
			}
		} else {
			//判断密码是否正确
			if user.Password == password {
				//判断是否为管理员登录
				if user.Id == 0 && user.Name == "000" && user.Password == "000000" {
					//管理员登录成功进入后台页面，解析index.html
					_, err := template.ParseFiles("./web/index.html")
					if err != nil {
						fmt.Println(err.Error())
					}

				} else {
					//登录成功进入首页，解析首页表单
					//http.Redirect(w,r,"./web/firstpage.html",http.StatusTemporaryRedirect)
					t, err := template.ParseFiles("./web/firstpage.html")
					if err != nil {
						fmt.Println(err.Error())
					}
					//{{.}}不需要赋值，也不需要显示出来
					err = t.Execute(w, "")
					if err != nil {
						log.Println(err.Error())
					}
				}
			} else {
				//给{{.}}赋值，说明登录失败，重新登录
				err = t1.Execute(w, "登录失败，请重新登录！")
				if err != nil {
					log.Println(err.Error())
				}
			}
		}
	}
}
