package controller

import (
	"accountingsystem/model"
	"accountingsystem/service"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func SelectExpensesByEDate(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		//解析SelectExpenses.html中的内容
		t1, err := template.ParseFiles("./web/SelectExpenses.html")
		if err != nil {
			log.Fatalln(err.Error())
		}
		//给待定值w赋值
		err = t1.Execute(w, "")
		if err != nil {
			log.Fatalln(err.Error())
		}
	} else {
		t2, err := template.ParseFiles("./web/SelectExpenses.html")
		if err != nil {
			log.Fatalln(err.Error())
		}

		//解析form表单
		err = r.ParseForm()
		if err != nil {
			log.Fatalln(err.Error())
		}
		//获取表单中的内容
		startdate := r.Form["startdate"][0]
		enddate := r.Form["enddate"][0]
		var result model.Expenses
		result = service.SelectExpensesByEDate(startdate, enddate)
		err = t2.Execute(w, result)
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

}

func SelectAllExpenses(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		//解析test.html中的内容
		t, err := template.ParseFiles("./web/test.html")
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
		result := service.SelectAllExpenses()
		//赋值给test.html中的待定值  将查询的支出记录赋给w显示出来
		err = t.Execute(w, result)
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
	}
}

func InsertExpenses(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		//解析expenses.html中的内容
		t, err := template.ParseFiles("./web/expenses.html")
		if err != nil {
			log.Fatalln(err.Error())
		}
		//{{.}}不需要赋值，也不需要显示出来
		err = t.Execute(w, "")
		if err != nil {
			log.Fatalln(err.Error())
		}
		return
	} else {
		//解析expenses.html
		t1, err := template.ParseFiles("./web/expenses.html")
		//解析form表单
		err = r.ParseForm()
		if err != nil {
			log.Fatalln(err.Error())
		}
		//获取表单中的内容
		uses := r.Form["uses"][0]
		emoney := r.Form["emoney"][0]
		//将获取内容赋值给结构体expense
		expense := model.Expenses{
			Uses:   uses,
			EMoney: emoney,
		}
		fmt.Println(expense)
		//判断是否添加成功
		if service.InsertExpenses(expense) {
			//t1, err := template.ParseFiles("./web/expenses.html")
			err = t1.Execute(w, "记录成功！")
			if err != nil {
				log.Fatalln(err.Error())
			}
		} else {
			//t1, err := template.ParseFiles("./web/expenses.html")
			err = t1.Execute(w, "记录失败，请重新输入！")
			if err != nil {
				log.Fatalln(err.Error())
			}
		}
	}
}


