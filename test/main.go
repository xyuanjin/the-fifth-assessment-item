package main

import (
	"accountingsystem/model"
	"accountingsystem/service"
	"accountingsystem/util"
	"fmt"
)

func main() {
	util.InitMysql()
	i:=model.Income{
		Id:        7,
		IMoney:    "100000",
		MoneyFrom: "123",
	}
	fmt.Println(service.InsertIncome(i))
}
