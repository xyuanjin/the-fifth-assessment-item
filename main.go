package main

import (
	"accountingsystem/controller"
	"accountingsystem/util"
	"net/http"
)

func main() {
	util.InitMysql()

	//注册账号
	http.HandleFunc("/register",controller.InsertUser)
	//登录账号
	http.HandleFunc("/login",controller.Login)

	//管理页面
	//查询一个用户信息
	http.HandleFunc("/selectuser",controller.SelectUserByUserName)
    //查询所有用户信息
	http.HandleFunc("/alluser",controller.SelectAllUser)

	//用户登录
	//修改信息
	http.HandleFunc("./updateuser",controller.UpdateUserByUsername)
	//记录支出
	http.HandleFunc("/expenses",controller.InsertExpenses)

	//启动服务器
	http.ListenAndServe(":8000",nil)
}
