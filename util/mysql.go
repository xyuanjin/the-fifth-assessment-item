package util

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func InitMysql() {
	var err error
	db, err = sql.Open("mysql", "root:102077@tcp(127.0.0.1:3306)/accounting?parseTime=true")
	if err != nil {
		fmt.Println("数据库连接失败，err:",err.Error())
		return
	}
	err = db.Ping()
	if err != nil {
		fmt.Println("数据库连接失败,err:",err.Error())
		return
	}
}

func GetCont() *sql.DB {
	_db:=db
	return _db
}