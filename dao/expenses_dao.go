package dao

import (
	"accountingsystem/model"
	"accountingsystem/util"
	"log"
)

func SelectExpensesByEDate(startdate,enddate string) model.Expenses{
	var expenses model.Expenses
	db := util.GetCont()
	contains := "select id,user_id,e_date,uses,e_money from t_expenses where e_date between ? and ?"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	row := stmt.QueryRow(startdate,enddate)
	if err != nil {
		log.Fatalln(err.Error())
	}
	err = row.Scan(&expenses.Id,&expenses.UserId,&expenses.EDate,&expenses.Uses,&expenses.EMoney)
	if err != nil {
		log.Fatalln(err.Error())
	}
	return expenses
}

func SelectAllExpenses() []model.Expenses {
	var expense model.Expenses
	expenses := make([]model.Expenses, 0)
	db := util.GetCont()
	contains := "Select * from t_expenses"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	rows, err := stmt.Query()
	if err != nil {
		log.Fatalln(err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&expense.Id,&expense.UserId,&expense.EDate,&expense.Uses,&expense.EMoney)
		if err != nil {
			log.Fatalln(err.Error())
		}
		expenses = append(expenses,expense)
	}
	return expenses
}

func InsertExpenses(expense model.Expenses) bool {
	result := true
	db := util.GetCont()
	contains :="insert into t_expenses (user_id,e_date,uses,e_money) values(?,?,?,?)"
	stmt,err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = stmt.Exec(expense.UserId,expense.EDate,expense.Uses,expense.EMoney)
	if err != nil {
		result = false
		log.Println(err.Error())
	}
	return result
}

func UpdateExpensesById(expense model.Expenses) bool {
	result := true
	db := util.GetCont()
	contains := "update t_expenses set e_date = ?,uses = ?,e_money = ? where id = ?;"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = stmt.Exec(expense.EDate,expense.Uses,expense.EMoney,expense.Id)
	if err != nil {
		result = false
		log.Fatalln(err.Error())
	}
	return result
}

func DeleteExpensesById(id int) bool {
	result := true
	db := util.GetCont()
	contains := "delete from t_expenses where id  = ?"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = stmt.Exec(id)
	if err != nil {
		result = false
		log.Println(err.Error())
	}
	return result
}