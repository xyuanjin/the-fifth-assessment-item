package dao

import (
	"accountingsystem/model"
	"accountingsystem/util"
	"fmt"
)

func InsertIncome(income model.Income) bool {
	db:=util.GetCont()
	if err:=db.Ping();err!=nil {
		fmt.Println(err.Error())
	}
	InsertSql:="insert into t_income (user_id,i_date,money_from,i_money) values(?,?,?,?)"
	stmt, _ := db.Prepare(InsertSql)
	stmt.Exec(income.Id,income.IDate,income.MoneyFrom, income.IMoney)
	return true
}