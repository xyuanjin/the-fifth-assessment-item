package dao

import (
	"accountingsystem/model"
	"accountingsystem/util"
	"database/sql"
	"fmt"
	"log"
	"runtime"
)

func SelectUserByUserName(username string ) model.User {
	var user model.User
	db := util.GetCont()
	contains := "select id,name,password from t_user where name = ?"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	row := stmt.QueryRow(username)
	if err != nil {
		log.Fatalln(err.Error())
	}
	err = row.Scan(&user.Id, &user.Name, &user.Password)
	switch {
	case err == sql.ErrNoRows:
	case err != nil:
		if _, file, line, ok := runtime.Caller(0); ok {
			fmt.Println(err, file, line)
		}
	}
	return user
}

func SelectAllUser() []model.User {
	var user model.User
	users := make([]model.User, 0)
	db := util.GetCont()
	contains := "Select * from t_user"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	rows, err := stmt.Query()
	if err != nil {
		log.Fatalln(err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&user.Id, &user.Name, &user.Password)
		if err != nil {
			log.Fatalln(err.Error())
		}
		users = append(users, user)
	}
	return users
}

func InsertUser(user model.User) bool {
	result := true
	db := util.GetCont()
	contains := "insert  into t_user(name,password)  values(?,?)"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = stmt.Exec(user.Name, user.Password)
	if err != nil {
		result = false
		log.Println(err.Error())
	}
	return result
}

func UpdateUserByUserName(user model.User) bool {
	result := true
	db := util.GetCont()
	contains := "update t_user set name = ?,password = ? where name = ?;"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = stmt.Exec(user.Name, user.Password, user.Name)
	if err != nil {
		result = false
		log.Fatalln(err.Error())
	}
	return result
}

func DeleteUserByUserName(username string) bool {
	result := true
	db := util.GetCont()
	contains := "delete from t_user where name = ?"
	stmt, err := db.Prepare(contains)
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = stmt.Exec(username)
	if err != nil {
		result = false
		log.Println(err.Error())
	}
	return result
}
