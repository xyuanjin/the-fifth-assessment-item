package model

import "reflect"

// User 用户
type User struct {
	Id       int    //用户id
	Name     string // 用户名
	Password string // 密码
}
// IsStructureEmpty
// 通过反射判断结构体是否为空
func (u User) IsStructureEmpty() bool {
	return reflect.DeepEqual(u, User{})
}
