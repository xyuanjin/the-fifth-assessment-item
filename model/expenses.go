package model

import (
	"reflect"
	"time"
)

// Expenses 支出
type Expenses struct {
	Id     int     //
	UserId int     //
	EDate  time.Time  //日期
	Uses   string  //支出用途
	EMoney string //支出金额
}

// IsStructureEmpty
// 通过反射判断结构体是否为空
func (e Expenses) IsStructureEmpty() bool {
	return reflect.DeepEqual(e, Expenses{})
}
