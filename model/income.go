package model

import (
	"reflect"
	"time"
)

//  Income 收入
type Income struct {
	Id        int       //用户id
	UserId    int       //记录序号
	IDate     time.Time //日期
	MoneyFrom string    //支出用途
	IMoney    string   //支出金额
}

// IsStructureEmpty
// 通过反射判断结构体是否为空
func (i Income) IsStructureEmpty() bool {
	return reflect.DeepEqual(i, Income{})
}
