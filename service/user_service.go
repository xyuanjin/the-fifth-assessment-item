package service

import (
	"accountingsystem/dao"
	"accountingsystem/model"
)

func SelectUserByUserName(username string ) model.User {
	return  dao.SelectUserByUserName(username)
}

func SelectAllUser()[]model.User{
	return dao.SelectAllUser()
}

func InsertUser(user model.User)bool{
	return dao.InsertUser(user)
}

func UpdateUserByUserName(user model.User) bool {
	return dao.UpdateUserByUserName(user)
}

func DeleteUserByUserName(username string ) bool{
	return dao.DeleteUserByUserName(username)
}