package service

import (
	"accountingsystem/dao"
	"accountingsystem/model"
	"time"
)

func InsertIncome(income model.Income) bool {
	income.IDate = time.Now().Local()
	dao.InsertIncome(income)
	return true
}
