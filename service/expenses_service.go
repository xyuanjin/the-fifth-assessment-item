package service

import (
	"accountingsystem/dao"
	"accountingsystem/model"
	"time"
)

func SelectExpensesByEDate(startdate,enddate string) model.Expenses{
	return dao.SelectExpensesByEDate(startdate,enddate)
}

func SelectAllExpenses() []model.Expenses{
	return dao.SelectAllExpenses()
}

func InsertExpenses(expense model.Expenses) bool{
	expense.UserId = 5
	expense.EDate = time.Now().Local()
	return dao.InsertExpenses(expense)
}

func UpdateExpensesById(expense model.Expenses) bool{
	return dao.UpdateExpensesById(expense)
}

func DeleteExpensesById(id int) bool{
	return dao.DeleteExpensesById(id)
}
